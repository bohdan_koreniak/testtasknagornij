package task;

import task.manager.ConsoleManager;

public class SolutionKing
{
    private int rowAmount;
    private int columnAmount;

    private char[][] chessboard;

    private int kingAmount;

    private int combinationNumber;

    private boolean isKingPresent(int row, int column)
    {
        return row >= 0 && row < rowAmount && column >= 0 && column < columnAmount && chessboard[row][column] == 'K';
    }

    private boolean isKingSafe(int row, int column)
    {
        for (int i = row - 1; i <= row + 1; i++)
        {
            for (int j = column - 1; j <= column + 1; j++)
            {
                if (isKingPresent(i, j))
                {
                    return false;
                }
            }
        }

        return true;
    }

    private void placeKing(int row, int column, int kingNumber)
    {
        if (isKingSafe(row, column))
        {
            chessboard[row][column] = 'K';

            int nextRow = row;
            int nextColumn = column + 1;

            if (nextColumn == columnAmount)
            {
                nextColumn = 0;
                nextRow++;
            }

            placeKings(nextRow, nextColumn, kingNumber + 1);
            chessboard[row][column] = '0';
        }
    }

    private void placeKings(int currentRow, int currentColumn, int currentKingNumber)
    {
        if (currentKingNumber == kingAmount)
        {
            combinationNumber++;
            showChessboard();

            return;
        }

        if (currentRow == rowAmount)
        {
            return;
        }

        for (int j = currentColumn; j < columnAmount; j++)
        {
            placeKing(currentRow, j, currentKingNumber);
        }

        for (int i = currentRow + 1; i < rowAmount; i++)
        {
            for (int j = 0; j < columnAmount; j++)
            {
                placeKing(i, j, currentKingNumber);
            }
        }
    }

    private void showChessboard()
    {
        System.out.println();
        System.out.println();
        System.out.println("Number of combination: " + combinationNumber);
        System.out.println();

        for (int i = 0; i < rowAmount; i++)
        {
            for (int j = 0; j < columnAmount; j++)
            {
                if (chessboard[i][j] != 'K')
                {
                    System.out.print("0" + "\t");
                }
                else
                {
                    System.out.print(chessboard[i][j] + "\t");
                }
            }
            System.out.println();
        }
    }

    public void run()
    {
        System.out.println("Please, enter the desired row amount of the chessboard");

        rowAmount = ConsoleManager.readInt();

        System.out.println("Enter the amount of chessboard columns");

        columnAmount = ConsoleManager.readInt();

        System.out.println("Enter the number of kings");

        kingAmount = ConsoleManager.readInt();

        this.chessboard = new char[rowAmount][columnAmount];

        System.out.println("Possible combinations of kings");

        placeKings(0, 0, 0);
    }

    public static void main(String[] args)
    {
        new SolutionKing().run();
    }
}
