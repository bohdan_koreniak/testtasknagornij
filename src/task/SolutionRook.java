package task;

import task.manager.ConsoleManager;

public class SolutionRook
{
    private int rowAmount;
    private int columnAmount;

    private char[][] chessboard;

    private int rookAmount;

    private int combinationNumber;

    /**
     *
     * @param rookPositions
     *  array with rook coordinates where I is a row number of the chessboard
     *  and rookPositions[I] is a column number of the chessboard
     * @param currentRookNumber
     *  number of the rook that is placing on the chessboard in this moment
     * @return
     *  array with rooks that don`t beat each other
     *  or NULL if such rook combination does not exists
     */
    private int[] placeRooks(int[] rookPositions, int currentRookNumber)
    {
        int totalAmountOfRooks = rookPositions.length;

        if (currentRookNumber == totalAmountOfRooks)
        {
            return rookPositions;
        }

        for (int i = 0; i < totalAmountOfRooks; i++)
        {
            boolean isPlacingCorrect = true;

            for (int j = 0; j < currentRookNumber; j++)
            {
                if (rookPositions[j] == i)
                {
                    isPlacingCorrect = false;
                    break;
                }
            }

            if (isPlacingCorrect)
            {
                rookPositions[currentRookNumber] = i;

                int[] rooks = placeRooks(rookPositions, currentRookNumber + 1);

                if (rooks != null)
                {
                    combinationNumber++;
                    showChessboard(rookPositions);
                }
            }
        }

        return null;
    }

    private void showChessboard(int[] rookPositions)
    {
        System.out.println();
        System.out.println();
        System.out.println("Number of combination: " + combinationNumber);
        System.out.println();

        for (int i = 0; i < rowAmount; i++)
        {
            for (int j = 0; j < columnAmount; j++)
            {
                chessboard[i][j] = '0';
            }
        }

        for (int i = 0; i < rookPositions.length; i++)
        {
            chessboard[i][rookPositions[i]] = 'R';
        }

        for (int i = 0; i < rowAmount; i++)
        {
            for (int j = 0; j < columnAmount; j++)
            {
                System.out.print(chessboard[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public void run()
    {
        while (true)
        {
            System.out.println("Please, enter the desired row amount of the chessboard");

            rowAmount = ConsoleManager.readInt();

            System.out.println("Enter the amount of chessboard columns");

            columnAmount = ConsoleManager.readInt();

            System.out.println("Enter the number of rooks");

            rookAmount = ConsoleManager.readInt();

            if (rookAmount > rowAmount || rookAmount > columnAmount)
            {
                System.out.println("Rook amount should be less than or equal to row amount and column amount. Enter data again");
            }
            else
            {
                break;
            }
        }

        this.chessboard = new char[rowAmount][columnAmount];

        System.out.println("Possible combinations of rooks");

        placeRooks(new int[rookAmount], 0);
    }

    public static void main(String[] args)
    {
        new SolutionRook().run();
    }
}
