package task;

import task.manager.ConsoleManager;
import task.manager.figure.KingManager;
import task.manager.figure.KnightManager;
import task.manager.figure.RookManager;

public class SolutionFigures
{
    private int rowAmount;
    private int columnAmount;

    private char[][] chessboard;

    private int kingAmount;
    private int rookAmount;
    private int knightAmount;

    private int combinationNumber;

    private KnightManager knightManager;
    private KingManager kingManager;
    private RookManager rookManager;

    public SolutionFigures()
    {
    }

    public SolutionFigures(int rowAmount, int columnAmount, char[][] chessboard, int kingAmount, int rookAmount, int knightAmount)
    {
        this.rowAmount = rowAmount;
        this.columnAmount = columnAmount;
        this.chessboard = chessboard;
        this.kingAmount = kingAmount;
        this.rookAmount = rookAmount;
        this.knightAmount = knightAmount;

        knightManager = new KnightManager(rowAmount, columnAmount, chessboard, knightAmount);
        kingManager = new KingManager(rowAmount, columnAmount, chessboard, kingAmount);
        rookManager = new RookManager(rowAmount, columnAmount, chessboard, rookAmount);
    }

    private void showChessboard()
    {
        System.out.println();
        System.out.println();
        System.out.println("Number of combination: " + combinationNumber);
        System.out.println();

        for (int i = 0; i < rowAmount; i++)
        {
            for (int j = 0; j < columnAmount; j++)
            {
                if (chessboard[i][j] != 'K' && chessboard[i][j] != 'R' && chessboard[i][j] != 'H')
                {
                    System.out.print("0" + "\t");
                }
                else
                {
                    System.out.print(chessboard[i][j] + "\t");
                }
            }
            System.out.println();
        }
    }

    public void run()
    {
        System.out.println("Please, enter the desired row amount of the chessboard");

        rowAmount = ConsoleManager.readPositiveInt();

        System.out.println("Enter the amount of chessboard columns");

        columnAmount = ConsoleManager.readPositiveInt();

        System.out.println("Enter the number of knights");

        knightAmount = ConsoleManager.readNonNegativeInt();

        System.out.println("Enter the number of kings");

        kingAmount = ConsoleManager.readNonNegativeInt();

        System.out.println("Enter the number of rooks");

        rookAmount = ConsoleManager.readNonNegativeInt();

        this.chessboard = new char[rowAmount][columnAmount];

        knightManager = new KnightManager(rowAmount, columnAmount, chessboard, knightAmount);
        kingManager = new KingManager(rowAmount, columnAmount, chessboard, kingAmount);
        rookManager = new RookManager(rowAmount, columnAmount, chessboard, rookAmount);

        System.out.println("Possible combinations of figures");

        StringBuilder strAmountCombinations = new StringBuilder();

        strAmountCombinations.append((int) Math.signum(rookAmount));
        strAmountCombinations.append((int) Math.signum(kingAmount));
        strAmountCombinations.append((int) Math.signum(knightAmount));

        switch (strAmountCombinations.toString())
        {
            case "111":
            {
                for (int i = 0; i < rowAmount; i++)
                {
                    rookManager.placeRooks(i, 0, onRookAmountReached ->
                    {
                        kingManager.placeKings(0, 0, 0, onKingAmountReached ->
                        {
                            knightManager.placeKnights(0, 0, 0, onKnightAmountReached ->
                            {
                                combinationNumber++;
                                showChessboard();
                            });
                        });
                    });
                }
                break;
            }
            case "110":
            {
                for (int i = 0; i < rowAmount; i++)
                {
                    rookManager.placeRooks(i, 0, onRookAmountReached ->
                    {
                        kingManager.placeKings(0, 0, 0, onKingAmountReached ->
                        {
                            combinationNumber++;
                            showChessboard();
                        });
                    });
                }
                break;
            }
            case "100":
            {
                for (int i = 0; i < rowAmount; i++)
                {
                    rookManager.placeRooks(i, 0, onRookAmountReached ->
                    {
                        combinationNumber++;
                        showChessboard();
                    });
                }
                break;
            }
            case "010":
            {
                kingManager.placeKings(0, 0, 0, onKingAmountReached ->
                {
                    combinationNumber++;
                    showChessboard();
                });
                break;
            }
            case "011":
            {
                kingManager.placeKings(0, 0, 0, onKingAmountReached ->
                {
                    knightManager.placeKnights(0, 0, 0, onKnightAmountReached ->
                    {
                        combinationNumber++;
                        showChessboard();
                    });
                });
                break;
            }
            case "001":
            {
                knightManager.placeKnights(0, 0, 0, onKnightAmountReached ->
                {
                    combinationNumber++;
                    showChessboard();
                });
                break;
            }
            case "000":
            {
                System.out.println("Figures amount is 0. Combinations can not be found");
                break;
            }
        }
    }

    public static void main(String[] args)
    {
        new SolutionFigures().run();
    }
}
