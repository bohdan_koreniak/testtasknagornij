package task.manager.figure;

import task.manager.figure.safety.FigureSafetyManager;

import java.util.function.Consumer;

public class RookManager
{
    private int rowAmount;
    private int columnAmount;

    private char[][] chessboard;

    private int rookAmount;

    private FigureSafetyManager figureSafetyManager;

    public RookManager(int rowAmount, int columnAmount, char[][] chessboard, int rookAmount)
    {
        this.rowAmount = rowAmount;
        this.columnAmount = columnAmount;
        this.chessboard = chessboard;
        this.rookAmount = rookAmount;

        figureSafetyManager = new FigureSafetyManager(rowAmount, columnAmount, chessboard);
    }

    public void placeRooks(int row, int rookNumber, Consumer onRookAmountReached)
    {
        for (int j = 0; j < columnAmount; j++)
        {
            if (figureSafetyManager.isRookSafe(row, j))
            {
                chessboard[row][j] = 'R';

                if (rookNumber + 1 == rookAmount)
                {
                    onRookAmountReached.accept(this);
                }
                else
                {
                    for (int i = row + 1; i < rowAmount; i++)
                    {
                        placeRooks(i, rookNumber + 1, onRookAmountReached);
                    }
                }

                chessboard[row][j] = '0';
            }
        }
    }
}
