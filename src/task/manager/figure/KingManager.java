package task.manager.figure;

import task.manager.figure.safety.FigureSafetyManager;

import java.util.function.Consumer;

public class KingManager
{
    private int rowAmount;
    private int columnAmount;

    private char[][] chessboard;

    private int kingAmount;

    private FigureSafetyManager figureSafetyManager;

    public KingManager(int rowAmount, int columnAmount, char[][] chessboard, int kingAmount)
    {
        this.rowAmount = rowAmount;
        this.columnAmount = columnAmount;
        this.chessboard = chessboard;
        this.kingAmount = kingAmount;

        figureSafetyManager = new FigureSafetyManager(rowAmount, columnAmount, chessboard);
    }

    private void placeKing(int row, int column, int kingNumber, Consumer onKingAmountReached)
    {
        if (figureSafetyManager.isKingSafe(row, column))
        {
            chessboard[row][column] = 'K';

            int nextRow = row;
            int nextColumn = column + 1;

            if (nextColumn == columnAmount)
            {
                nextColumn = 0;
                nextRow++;
            }

            placeKings(nextRow, nextColumn, kingNumber + 1, onKingAmountReached);
            chessboard[row][column] = '0';
        }
    }

    public void placeKings(int currentRow, int currentColumn, int currentKingNumber, Consumer onKingAmountReached)
    {
        if (currentKingNumber == kingAmount)
        {
            onKingAmountReached.accept(this);
            return;
        }

        if (currentRow == rowAmount)
        {
            return;
        }

        for (int j = currentColumn; j < columnAmount; j++)
        {
            placeKing(currentRow, j, currentKingNumber, onKingAmountReached);
        }

        for (int i = currentRow + 1; i < rowAmount; i++)
        {
            for (int j = 0; j < columnAmount; j++)
            {
                placeKing(i, j, currentKingNumber, onKingAmountReached);
            }
        }
    }
}
