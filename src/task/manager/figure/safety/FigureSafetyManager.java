package task.manager.figure.safety;

public class FigureSafetyManager
{
    private int rowAmount;
    private int columnAmount;

    private char[][] chessboard;

    public FigureSafetyManager(int rowAmount, int columnAmount, char[][] chessboard)
    {
        this.rowAmount = rowAmount;
        this.columnAmount = columnAmount;
        this.chessboard = chessboard;
    }

    public boolean isFigurePresent(int row, int column)
    {
        return row >= 0 && row < rowAmount && column >= 0 && column < columnAmount &&
                (chessboard[row][column] == 'H' || chessboard[row][column] == 'K' || chessboard[row][column] == 'R');
    }

    public boolean isKnightPresent(int row, int column)
    {
        return row >= 0 && row < rowAmount && column >= 0 && column < columnAmount && chessboard[row][column] == 'H';
    }

    public boolean isKingPresent(int row, int column)
    {
        return row >= 0 && row < rowAmount && column >= 0 && column < columnAmount && chessboard[row][column] == 'K';
    }

    public boolean isRookPresent(int row, int column)
    {
        return row >= 0 && row < rowAmount && column >= 0 && column < columnAmount && chessboard[row][column] == 'R';
    }

    public boolean isKnightSafe(int row, int column)
    {
        if (isFigurePresent(row - 1, column - 2) || isFigurePresent(row - 1, column + 2) || isFigurePresent(row + 1, column - 2)
                || isFigurePresent(row + 1, column + 2) || isFigurePresent(row - 2, column - 1) || isFigurePresent(row - 2, column + 1)
                || isFigurePresent(row + 2, column - 1) || isFigurePresent(row + 2, column + 1))
        {
            return false;
        }

        for (int i = row - 1; i <= row + 1; i++)
        {
            for (int j = column - 1; j <= column + 1; j++)
            {
                if (isKingPresent(i, j))
                {
                    return false;
                }
            }
        }

        for (int j = 0; j < columnAmount; j++)
        {
            if (isRookPresent(row, j))
            {
                return false;
            }
        }

        for (int i = 0; i < rowAmount; i++)
        {
            if (isRookPresent(i, column))
            {
                return false;
            }
        }

        return true;
    }

    public boolean isKingSafe(int row, int column)
    {
        if (isKnightPresent(row - 1, column - 2) || isKnightPresent(row - 1, column + 2) || isKnightPresent(row + 1, column - 2)
                || isKnightPresent(row + 1, column + 2) || isKnightPresent(row - 2, column - 1) || isKnightPresent(row - 2, column + 1)
                || isKnightPresent(row + 2, column - 1) || isKnightPresent(row + 2, column + 1))
        {
            return false;
        }

        for (int i = row - 1; i <= row + 1; i++)
        {
            for (int j = column - 1; j <= column + 1; j++)
            {
                if (isFigurePresent(i, j))
                {
                    return false;
                }
            }
        }

        for (int j = 0; j < columnAmount; j++)
        {
            if (isRookPresent(row, j))
            {
                return false;
            }
        }

        for (int i = 0; i < rowAmount; i++)
        {
            if (isRookPresent(i, column))
            {
                return false;
            }
        }

        return true;
    }

    public boolean isRookSafe(int row, int column)
    {
        if (isKnightPresent(row - 1, column - 2) || isKnightPresent(row - 1, column + 2) || isKnightPresent(row + 1, column - 2)
                || isKnightPresent(row + 1, column + 2) || isKnightPresent(row - 2, column - 1) || isKnightPresent(row - 2, column + 1)
                || isKnightPresent(row + 2, column - 1) || isKnightPresent(row + 2, column + 1))
        {
            return false;
        }

        for (int i = row - 1; i <= row + 1; i++)
        {
            for (int j = column - 1; j <= column + 1; j++)
            {
                if (isKingPresent(i, j))
                {
                    return false;
                }
            }
        }

        for (int j = 0; j < columnAmount; j++)
        {
            if (isFigurePresent(row, j))
            {
                return false;
            }
        }

        for (int i = 0; i < rowAmount; i++)
        {
            if (isFigurePresent(i, column))
            {
                return false;
            }
        }

        return true;
    }
}
