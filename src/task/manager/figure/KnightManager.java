package task.manager.figure;

import task.manager.figure.safety.FigureSafetyManager;

import java.util.function.Consumer;

public class KnightManager
{
    private int rowAmount;
    private int columnAmount;

    private char[][] chessboard;

    private int knightAmount;

    private FigureSafetyManager figureSafetyManager;

    public KnightManager(int rowAmount, int columnAmount, char[][] chessboard, int knightAmount)
    {
        this.rowAmount = rowAmount;
        this.columnAmount = columnAmount;
        this.chessboard = chessboard;
        this.knightAmount = knightAmount;

        figureSafetyManager = new FigureSafetyManager(rowAmount, columnAmount, chessboard);
    }

    private void placeKnight(int row, int column, int knightNumber, Consumer onKnightAmountReached)
    {
        if (figureSafetyManager.isKnightSafe(row, column))
        {
            chessboard[row][column] = 'H';

            int nextRow = row;
            int nextColumn = column + 1;

            if (nextColumn == columnAmount)
            {
                nextColumn = 0;
                nextRow++;
            }

            placeKnights(nextRow, nextColumn, knightNumber + 1, onKnightAmountReached);
            chessboard[row][column] = '0';
        }
    }

    public void placeKnights(int currentRow, int currentColumn, int currentKnightNumber, Consumer onKnightAmountReached)
    {
        if (currentKnightNumber == knightAmount)
        {
            onKnightAmountReached.accept(this);
            return;
        }

        if (currentRow == rowAmount)
        {
            return;
        }

        for (int j = currentColumn; j < columnAmount; j++)
        {
            placeKnight(currentRow, j, currentKnightNumber, onKnightAmountReached);
        }

        for (int i = currentRow + 1; i < rowAmount; i++)
        {
            for (int j = 0; j < columnAmount; j++)
            {
                placeKnight(i, j, currentKnightNumber, onKnightAmountReached);
            }
        }
    }
}
