package task.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleManager
{
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public static int readInt()
    {
        try
        {
            return Integer.parseInt(bufferedReader.readLine());
        }
        catch (IOException | NumberFormatException e)
        {
            System.out.println("Enter number in the right format");
            return readInt();
        }
    }

    public static int readPositiveInt()
    {
        try
        {
            int number = Integer.parseInt(bufferedReader.readLine());

            if (number <= 0)
            {
                System.out.println("Number should be positive");
                return readPositiveInt();
            }

            return number;
        }
        catch (IOException | NumberFormatException e)
        {
            System.out.println("Enter number in the right format");
            return readPositiveInt();
        }
    }

    public static int readNonNegativeInt()
    {
        try
        {
            int number = Integer.parseInt(bufferedReader.readLine());

            if (number < 0)
            {
                System.out.println("Number should be positive or be equal to 0");
                return readNonNegativeInt();
            }

            return number;
        }
        catch (IOException | NumberFormatException e)
        {
            System.out.println("Enter number in the right format");
            return readNonNegativeInt();
        }
    }

    public static String readString()
    {
        try
        {
            return bufferedReader.readLine();
        }
        catch (IOException e)
        {
            System.out.println("Error while reading string from console. Try again");
            return readString();
        }
    }

    public static void showMessage(String message)
    {
        System.out.println(message);
    }
}
